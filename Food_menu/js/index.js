window.addEventListener('DOMContentLoaded', () => {
    // Tabs
    const tabs = document.querySelectorAll('.tabheader__item'),
          tabsContent = document.querySelectorAll('.tabcontent'),
          tabsParent = document.querySelector('.tabheader__items');

    function hideTabContent() {
        tabsContent.forEach(item => {
            /* item.style.display = 'none'; */
            item.classList.add('hide');
            item.classList.remove('show', 'fade');
        });
        tabs.forEach(item => {
            item.classList.remove('tabheader__item_active');
        });
    }

    function showTabContent(i = 2) {
        /* tabsContent[i].style.display = 'block'; */
        tabsContent[i].classList.add('show', 'fade');
        tabsContent[i].classList.remove('hide');
        tabs[i].classList.add('tabheader__item_active');
    }

    hideTabContent();
    showTabContent();

    tabsParent.addEventListener('click', (event) => {
        const target = event.target;
        if (target && target.classList.contains('tabheader__item')) {
            tabs.forEach((item, i) => {
                if (target == item) {
                    hideTabContent();
                    showTabContent(i);
                }
            });
        }
    });

    // Timer

    const deadline = '2022-12-30';
    function getTimeRemaining(endTime) {
        const differenseTime = Date.parse(endTime) - Date.parse(new Date()),
              days = Math.floor(differenseTime / (1000 * 60 * 60 *24)),
              hours = Math.floor(differenseTime / (1000 * 60 * 60) %  24),
              minutes = Math.floor((differenseTime / 1000 / 60) % 60),
              seconds = Math.floor((differenseTime / 1000) % 60);
        return {
            total: differenseTime,
            days,
            hours,
            minutes,
            seconds
        };
    }

    function getZero(num) {
        if (num >= 0 && num < 10) {
            return `0${num}`;
        } else {
            return num;
        }
    }

    function setClock(selector, endTime) {
        const timer = document.querySelector(selector),
              days = timer.querySelector('#days'),
              hours = timer.querySelector('#hours'),
              minutes = timer.querySelector('#minutes'),
              seconds = timer.querySelector('#seconds'),
              timeInterval = setInterval (updateClock, 1000);

        updateClock();

        function updateClock() {
           const differenseTime = getTimeRemaining(endTime);
           
           days.innerHTML = getZero(differenseTime.days);
           hours.innerHTML = getZero(differenseTime.hours);
           minutes.innerHTML = getZero(differenseTime.minutes);
           seconds.innerHTML = getZero(differenseTime.seconds);

           if (differenseTime.total <= 0) {
            clearInterval(timeInterval);
           }
        }
    }

    setClock('.timer', deadline)

});